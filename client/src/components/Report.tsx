import classes from "./Report.module.css"

interface Props {
    detailedReport: string;
    code: string;
    lines: string;
}

function Report({detailedReport, code, lines}: Props) {

    return (
        <div className={"card " + classes.reportWrapper}>
            <div className="card-body">
                <h5 className="card-title">Report of the Code</h5>
                <h6 className="card-subtitle mb-2 text-body-secondary">
                    {detailedReport == "true" ? "Detailed Report" : "Regular Report"}
                </h6>
                <p className="card-text">{code}</p>
                {  
                    detailedReport == "true" ? 
                    <div>
                        <span className="card-link">Lines: </span>
                        <span className="card-link">{lines}</span>
                    </div>
                    : ""
                }
            </div>
        </div>
    );
}

export default Report;