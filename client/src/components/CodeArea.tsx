
import { useRef, useState } from 'react';
import classes from './CodeArea.module.css'
import Report from "./Report";

function CodeArea(){
    
    const codeRef = useRef(null);
    const checkBoxRef = useRef(null);
    
    const [dataLoaded, setDataLoaded] = useState(false);
    const [loadedData, setLoadedData] = useState({code: "", checkBox: "", lines: 0});
    const [dataLoading, setDataLoading] = useState(false);


    const handleCount = async () => {
        const decoder = new TextDecoder('utf-8');
        setDataLoading(true);

        if(codeRef.current && checkBoxRef.current){ // avoid null values
            const data = {
                code: codeRef.current["value"],
                checkBox: checkBoxRef.current["checked"]
            };
    
            fetch("https://backend-form-app.app.cern.ch/sendData", {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                method: 'POST',
                body: JSON.stringify(data)
            }).then(res => {
                if(res.body){
                    res.body
                    .getReader()
                    .read()
                    .then(({value}) => {
                        const responseData = JSON.parse(decoder.decode(value));
                        setDataLoaded(true)
                        setLoadedData(responseData);
                        console.log(responseData);
                    });
                    setDataLoading(false);
                }
            }).catch(err => {
                setDataLoading(false);
                console.log("Server is down");
                console.log(err.name + ": " + err.message);
            });
        }
    }

    return (
        <>
            <div className={classes.sectionWrapper}>
                <div className="mb-3">
                    <label className="form-label"><h3>Code Info & Report</h3></label>
                </div>
                <div className="mb-3">
                    <label className="form-label">Enter Code</label>
                    <textarea ref={codeRef} className="form-control" id="exampleFormControlTextarea1" rows={10}></textarea>
                </div>
                <div className="mb-3 form-check">
                    <input ref={checkBoxRef} type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label">Detailed Report</label>
                </div>
                <button type="submit" className="btn btn-primary" onClick={handleCount}>Submit</button>
                <br></br>
                {dataLoading ? <span>Loading...</span> : null}
            </div>

            {
                dataLoaded ? <Report detailedReport={loadedData["checkBox"]} code={loadedData["code"]} lines={loadedData["lines"].toString()}/> : null
            }
        </>
    );
}

export default CodeArea;