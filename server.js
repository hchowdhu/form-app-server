const express = require("express");
const app = express();
const path = require("path");
const cors = require("cors");

// Define the static path
app.use(express.static(path.join(__dirname, "client", "dist")));

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "client", "dist", "index.html"));
});

app.listen(8000, () => {
    console.log("Server has started on port 8000");
})